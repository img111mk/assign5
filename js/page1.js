/* 1.	When a mouse is above any paragraph, 
change the background color of the 
paragraph to red. */


var paragraphs = document.getElementsByTagName('P');
// Loop through them
for(var i = 0; i < paragraphs.length; i++) {
    // Add the "mouseover" event listener
    paragraphs[i].addEventListener("mouseover", function(event) {
        // Set the current target background color
        event.target.style.backgroundColor = "red";    
    });
    // Add the "mouseleave" event listener
    paragraphs[i].addEventListener("mouseleave", function(event) {
        // Reset the current target background color
        event.target.style.backgroundColor = "";    
    });
}

// 2.	When a mouse is above any table cell, change its background color to red.

//grabe th table element tags into an array
var tableCells = document.getElementsByTagName("td");

//create loop
for(var i = 0; i < tableCells.length; i++) {
//add mouseover listener
tableCells[i].addEventListener("mouseover", function(event) {
   event.target.style.backgroundColor = "red";    
  });
//add mouse out listener
tableCells[i].addEventListener("mouseleave", function(event) {
  event.target.style.backgroundColor = "white";    
  });
} 


//3.	When you click on any image, change its width to 50 px.  On the next click, 
//restore the image to its original size.


//get the image ids 
var images = document.getElementsByTagName('img');

//create loop
for(var i = 0; i < images.length; i++) {

//add onclick 
images[i].addEventListener("click", function(event) {
 if (images.width="150px"){ 
   event.target.width = "50";
  }
else 
 {
   event.target.width = "150"; 
}

});
}

